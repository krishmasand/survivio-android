package gg.krish.survive;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.webkit.WebView;

import io.github.controlwear.virtual.joystick.android.JoystickView;

public class GameView extends WebView {

    private static final float DEFAULT_DISTANCE_SHOOT = 50f;
    private static final int DEFAULT_EVENT_META = 0;

    public GameView(Context context) {
        super(context);
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (Toggles.SHOOT_JOYSTICK) {
            return super.onTouchEvent(event);
        }
        int action = event.getAction();
        if (action == MotionEvent.ACTION_DOWN) {
            this.dispatchHoverEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_HOVER_ENTER, event.getX(), event.getY(), event.getMetaState()));
            this.dispatchHoverEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_HOVER_MOVE, event.getX(), event.getY(), event.getMetaState()));
            this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_SPACE));
        } else if (action == MotionEvent.ACTION_MOVE){
            this.dispatchHoverEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_HOVER_MOVE, event.getX(), event.getY(), event.getMetaState()));
        } else if (action == MotionEvent.ACTION_UP) {
            this.dispatchHoverEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_HOVER_EXIT, event.getX(), event.getY(), event.getMetaState()));
            this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_SPACE));
        }

        performClick();
        return super.onTouchEvent(event);
    }

    public void configureShootingJoystick(int horScreenSize, int vertScreenSize, JoystickView joystickView){
        final GameView gameView = this;
        final float centerX = (float) horScreenSize / 2F;
        final float centerY = (float) vertScreenSize / 2F;

        final Handler releaseSpaceHandler = new Handler();

        joystickView.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {
                if (strength > 20) {
                    final float x = centerX + (DEFAULT_DISTANCE_SHOOT * (float) Math.cos(Math.toRadians(angle)));
                    final float y = centerY - (DEFAULT_DISTANCE_SHOOT * (float) Math.sin(Math.toRadians(angle)));
                    gameView.dispatchHoverEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_HOVER_ENTER, x, y, DEFAULT_EVENT_META));
                    gameView.dispatchHoverEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_HOVER_MOVE, x, y, DEFAULT_EVENT_META));
                    gameView.dispatchHoverEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_HOVER_EXIT, x, y, DEFAULT_EVENT_META));
                    gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_SPACE));
                    releaseSpaceHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_SPACE));
                        }
                    }, 5);
                }
            }
        });
    }
}
