package gg.krish.survive;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.common.base.Charsets;
import com.google.common.io.CharSink;
import com.google.common.io.Files;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GameWebViewClient extends WebViewClient{

    private static final String SCRIPT_PATTERN_STRING = "http.*surviv.*io.*app.*js";
    private static final Pattern SCRIPT_PATTERN = Pattern.compile(SCRIPT_PATTERN_STRING);

    private static final String SHOOT_START_PATTERN_STRING = "shootStart=.\\.mousePressed\\(\\)";
    private static final Pattern SHOOT_START_PATTERN = Pattern.compile(SHOOT_START_PATTERN_STRING);

    private Context context;

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, final WebResourceRequest request) {
        Handler mainHandler = new Handler(context.getMainLooper());
        final String url = request.getUrl().toString();
        if (SCRIPT_PATTERN.matcher(url).find()) {
            WebResourceResponse response = getAddSpaceToShootScript(url);
            if (response != null) {
                return response;
            } else {
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "May not have found local script", Toast.LENGTH_LONG).show();
                        System.out.println("km> " + request.getUrl().toString());
                    }
                });
            }
        }
        return super.shouldInterceptRequest(view, request);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private WebResourceResponse getAddSpaceToShootScript(final String endpoint) {
        File scriptFile = downloadFile(endpoint);
        if (scriptFile == null) {
            return null;
        }

        String content = readFromFile(scriptFile);
        if (content == null) {
            return null;
        }

        Matcher matcher = SHOOT_START_PATTERN.matcher(content);
        String substring;
        int endIndex;
        if (matcher.find()) {
            substring = matcher.group();
            endIndex = matcher.end();
        } else {
            return null;
        }
        char letter = substring.charAt(substring.indexOf("shootStart") + "shoot.Start".length());
        char letter2 = content.charAt(content.indexOf(".Key.R", endIndex) -1);
        String newString = substring + "||" + letter + ".keyPressed(" + letter2 + ".Key.Space)";
        String newContent = content.replace(substring, newString);

        boolean wrote = writeFile(newContent, scriptFile);
        if (!wrote) {
            return null;
        }

        return getAppScriptResponse(scriptFile);
    }

    private File downloadFile(final String endpoint) {
        File file;
        try {
            URL url = new URL(endpoint);

            URLConnection ucon = url.openConnection();
            ucon.setReadTimeout(5000);
            ucon.setConnectTimeout(10000);

            InputStream is = ucon.getInputStream();
            BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);

            file = new File(context.getDir("surviv", Context.MODE_PRIVATE) + "/script.js");

            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();

            FileOutputStream outStream = new FileOutputStream(file);
            byte[] buff = new byte[5 * 1024];

            int len;
            while ((len = inStream.read(buff)) != -1) {
                outStream.write(buff, 0, len);
            }

            outStream.flush();
            outStream.close();
            inStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return file;
    }

    private String readFromFile(File file) {
        String ret = null;
        try {
            String contents = Files.asCharSource(file, Charsets.UTF_8).read();
            if (contents != null) {
                ret = contents;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    private boolean writeFile(String str, File file) {
        try {
            CharSink sink = Files.asCharSink(file, Charsets.UTF_8);
            sink.write(str);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private WebResourceResponse getAppScriptResponse(File file) {
        String mimeType = "application/javascript";
        String encoding = "UTF-8";
        try {
            InputStream inputStream = new FileInputStream(file);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int statusCode = 200;
                String reasonPhase = "OK";
                Map<String, String> responseHeaders = new HashMap<>();
                responseHeaders.put("Access-Control-Allow-Origin", "*");
                return new WebResourceResponse(mimeType, encoding, statusCode, reasonPhase, responseHeaders, inputStream);
            }
            return new WebResourceResponse(mimeType, encoding, inputStream);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
