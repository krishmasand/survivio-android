package gg.krish.survive;

import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;

import io.github.controlwear.virtual.joystick.android.JoystickView;

public class MainActivity extends AppCompatActivity {

    private static String DEFAULT_URL = "http://surviv.io/";

    private Button fBtn;
    private Button rBtn;
    private Button escapeBtn;

    private JoystickView joystickLeft;
    private JoystickView joystickRight;

    private GameView gameView;
    private RelativeLayout relativeLayout;
    private ViewGroup content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        String url = DEFAULT_URL;
        Intent intent = getIntent();
        Uri data = intent.getData();
        if (data != null) {
            url = data.toString();
        }

        content = findViewById(android.R.id.content);
        relativeLayout = findViewById(R.id.layout);

        gameView = findViewById(R.id.webview);
        gameView.setWebViewClient(new WebViewClient());
        gameView.getSettings().setJavaScriptEnabled(true);

        fBtn = findViewById(R.id.f_btn);
        rBtn = findViewById(R.id.r_btn);
        escapeBtn = findViewById(R.id.escape_btn);
        Button shootSwitchBtn = findViewById(R.id.shoot_switch_btn);

        joystickLeft = findViewById(R.id.joystick_left);
        joystickRight = findViewById(R.id.joystick_right);

        setGoFullscreenAfterKeyboardHide();
        setupKeyboardButtons();

        shootSwitchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toggles.SHOOT_JOYSTICK = !Toggles.SHOOT_JOYSTICK;
                if (!Toggles.SHOOT_JOYSTICK) {
                    joystickRight.setVisibility(View.GONE);
                } else {
                    joystickRight.setVisibility(View.VISIBLE);
                }
            }
        });

        setupMovementJoystick();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        gameView.configureShootingJoystick(width, height, joystickRight);
        GameWebViewClient gameWebViewClient = new GameWebViewClient();
        gameWebViewClient.setContext(this);
        gameView.setWebViewClient(gameWebViewClient);
        gameView.loadUrl(url);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            goFullscreen();
        }
    }

    private void goFullscreen() {
        relativeLayout.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    private void setGoFullscreenAfterKeyboardHide() {
        content.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                content.getWindowVisibleDisplayFrame(r);
                int screenHeight = content.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                }
                else {
                    // keyboard is closed
                    goFullscreen();
                }
            }
        });
    }

    private void resetButtons() {
        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_W));
        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_A));
        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_S));
        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_D));
    }

    private void setupKeyboardButtons() {

        final Handler releaseFHandler = new Handler();
        fBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_F));
                releaseFHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_F));
                    }
                }, 10);
            }
        });

        final Handler releaseRHandler = new Handler();
        rBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_R));
                releaseRHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_R));
                    }
                }, 10);
            }
        });

        escapeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ESCAPE));
                gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ESCAPE));
            }
        });
    }

    private void setupMovementJoystick() {
        joystickLeft.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {
                if (strength > 20) {
                    if (angle <= 23 || angle > 338) {
                        resetButtons();
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_D));
                    } else if (angle <= 68) {
                        resetButtons();
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_D));
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_W));
                    } else if (angle <= 113) {
                        resetButtons();
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_W));
                    } else if (angle <= 158) {
                        resetButtons();
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_W));
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_A));
                    } else if (angle <= 203) {
                        resetButtons();
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_A));
                    } else if (angle <= 248) {
                        resetButtons();
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_A));
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_S));
                    } else if (angle <= 293) {
                        resetButtons();
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_S));
                    } else if (angle <= 338) {
                        resetButtons();
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_S));
                        gameView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_D));
                    }
                } else {
                    resetButtons();
                }
            }
        });
    }
}
